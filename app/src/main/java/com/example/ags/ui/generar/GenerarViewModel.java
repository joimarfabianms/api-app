package com.example.ags.ui.generar;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GenerarViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public GenerarViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Próximamente disponible");
    }

    public LiveData<String> getText() {
        return mText;
    }
}