package com.example.ags.ui.solicitar;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SolicitarViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SolicitarViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Selecciona el tipo de solicitud a realizar");
    }

    public LiveData<String> getText() {
        return mText;
    }
}