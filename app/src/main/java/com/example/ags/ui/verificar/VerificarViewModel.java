package com.example.ags.ui.verificar;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class VerificarViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public VerificarViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Verifica un pedido");
    }

    public LiveData<String> getText() {
        return mText;
    }
}