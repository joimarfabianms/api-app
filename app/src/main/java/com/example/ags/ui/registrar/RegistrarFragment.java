package com.example.ags.ui.registrar;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.example.ags.MainActivity;
import com.example.ags.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Console;
import java.util.Objects;

public class RegistrarFragment extends Fragment {

    private RegistrarViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(RegistrarViewModel.class);

        View root = inflater.inflate(R.layout.fragment_registrar, container, false);


        return root;
    }


}