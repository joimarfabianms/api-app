package com.example.ags.ui.registrar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ags.R;

public class Registrar2 extends AppCompatActivity {
    Button buttonCheckPack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar2);
        buttonCheckPack = findViewById(R.id.btn_añadir_paquete);
        confirmarPaquete();


    }

    public void confirmarPaquete(){

        buttonCheckPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "Paquete añadido", Toast.LENGTH_SHORT);
                toast1.show();
                startActivity(new Intent(Registrar2.this, Registrar1.class));

            }
        });
    }
}
