package com.example.ags.ui.agregar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ags.R;
import com.example.ags.ui.generar.GenerarViewModel;

public class AgregarUsuarioFragment extends Fragment {

    private GenerarViewModel generarViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        generarViewModel =
                ViewModelProviders.of(this).get(GenerarViewModel.class);
        View root = inflater.inflate(R.layout.fragment_agregar_usuario, container, false);
        final TextView textView = root.findViewById(R.id.text_generar);

        return root;
    }
}