package com.example.ags.ui.devolver;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DevolverViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DevolverViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is devolver fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}