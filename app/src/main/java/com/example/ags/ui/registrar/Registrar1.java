package com.example.ags.ui.registrar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ags.MainActivity;
import com.example.ags.R;

import java.util.Calendar;

public class Registrar1 extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    Button buttonAddPack;
    EditText editFecha;
    Button buttonConfirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar1);
        buttonConfirmar = findViewById(R.id.btn_confirmar);
        buttonAddPack = (Button) findViewById(R.id.btn_add_pack);
        añadirPaquete();
        confirmarPedido();
        editFecha = findViewById(R.id.edit_fecha);
        editFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
    }

    public void confirmarPedido() {
        buttonConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "Pedido confirmado", Toast.LENGTH_LONG);

                toast1.show();
                startActivity(new Intent(Registrar1.this, MainActivity.class));
            }
        });

    }

    public void añadirPaquete() {
        buttonAddPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registrar1.this, Registrar2.class));

            }
        });
    }

    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "/" + month + "/" + year;
        editFecha.setText(date);
    }
}
