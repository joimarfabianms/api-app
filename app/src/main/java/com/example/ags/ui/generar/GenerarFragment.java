package com.example.ags.ui.generar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ags.R;

public class GenerarFragment extends Fragment {

    private GenerarViewModel generarViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        generarViewModel =
                ViewModelProviders.of(this).get(GenerarViewModel.class);
        View root = inflater.inflate(R.layout.fragment_generar, container, false);
        final TextView textView = root.findViewById(R.id.text_generar);
        generarViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}