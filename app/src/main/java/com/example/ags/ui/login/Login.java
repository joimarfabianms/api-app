package com.example.ags.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ags.MainActivity;
import com.example.ags.R;

public class Login extends AppCompatActivity {
    Button login;
    EditText usuario,contraseña;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Declaración de componentes graficos en el activity
        login = findViewById(R.id.btn_login);
        usuario =  findViewById(R.id.editTextUsuario);
        contraseña = findViewById(R.id.editTextContraseña);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarEntrada()) {
                    startActivity(new Intent(Login.this, MainActivity.class));
                }
            }
        });
    }


    //Metodo para validar el acceso a la app
    public boolean validarEntrada(){
        // Variable que guarda si todo coincide correctamente
        boolean accesoConcedido=false;

        String user = usuario.getText().toString();
        String password = contraseña.getText().toString();
        if(!user.isEmpty()&&!password.isEmpty()){

            accesoConcedido=true;
            Toast toast1 = Toast.makeText(getApplicationContext(),
                    "Inicio de sesión exitoso", Toast.LENGTH_LONG);
            toast1.show();

        }

        if(user.isEmpty()&&password.isEmpty()){
            accesoConcedido=false;
            Toast toast1 =Toast.makeText(getApplicationContext(),"Completa todos los campos", Toast.LENGTH_LONG);
            toast1.show();
        }

        return accesoConcedido;
    }
}
